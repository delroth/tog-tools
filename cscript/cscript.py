"""
cscript.py
~~~~~~~~

An IDAPython processor module for the CScript bytecode.

Copyright (C) 2012 Pierre Bourdon <delroth@lse.epita.fr>

Licensed under the GPLv2 license, see the LICENSE file at the root of this
repository.
"""

from idaapi import *

import struct

class DecodingError(Exception):
    pass

class CScriptProcessor(processor_t):
    id = 0x8000 + 5453
    flag = PR_ADJSEGS | PRN_HEX
    cnbits = 8
    dnbits = 8
    psnames = ["cscript"]
    plnames = ["CScript bytecode"]
    segreg_size = 0

    assembler = {
        "flag" : AS_NCHRE | ASH_HEXF4 | ASD_DECF1 | ASO_OCTF3 | ASB_BINF2
               | AS_NOTAB | AS_ASCIIC | AS_ASCIIZ,
        "uflag": 0,
        "name": "CScript assembler",

        "origin": ".org",
        "end": "end",
        "cmnt": ";",

        "ascsep": '"',
        "accsep": "'",
        "esccodes": "\"'",

        "a_ascii": ".ascii",
        "a_byte": ".byte",
        "a_word": ".word",
        "a_dword": ".dword",

        "a_bss": "dfs %s",

        "a_seg": "seg",
        "a_curip": ".",
        "a_public": "",
        "a_weak": "",
        "a_extrn": ".extern",
        "a_comdef": "",
        "a_align": ".align",

        "lbrace": "(",
        "rbrace": ")",
        "a_mod": "%",
        "a_band": "&",
        "a_bor": "|",
        "a_xor": "^",
        "a_bnot": "~",
        "a_shl": "<<",
        "a_shr": ">>",
        "a_sizeof_fmt": "size %s",
    }

    reg_names = regNames = [
        "CS", "DS"
    ]

    instruc = instrs = [
        { 'name': 'ADD', 'feature': 0 },
        { 'name': 'SUB', 'feature': 0 },
        { 'name': 'MUL', 'feature': 0 },
        { 'name': 'DIV', 'feature': 0 },
        { 'name': 'MOD', 'feature': 0 },
        { 'name': 'ADD_NOUP', 'feature': 0 },
        { 'name': 'SUB_NOUP', 'feature': 0 },
        { 'name': 'OR', 'feature': 0 },
        { 'name': 'SHR', 'feature': 0 },
        { 'name': 'AND', 'feature': 0 },

        { 'name': 'CAST', 'feature': CF_USE1 },

        { 'name': 'CMPEQ', 'feature': 0 },
        { 'name': 'CMPNE', 'feature': 0 },
        { 'name': 'CMPLE', 'feature': 0 },
        { 'name': 'CMPGE', 'feature': 0 },
        { 'name': 'CMPGT', 'feature': 0 },
        { 'name': 'CMPLT', 'feature': 0 },

        { 'name': 'CALL', 'feature': CF_USE1 | CF_CALL },
        { 'name': 'SYSCALL', 'feature': CF_USE1 | CF_USE2 },
        { 'name': 'RET', 'feature': CF_USE1 },
        { 'name': 'J', 'feature': CF_USE1 | CF_STOP },
        { 'name': 'JZ', 'feature': CF_USE1 },

        { 'name': 'INDEX', 'feature': 0 },
        { 'name': 'RAWINDEX', 'feature': CF_USE1 | CF_USE2 | CF_USE3 },
        { 'name': 'DEREF', 'feature': 0 },
        { 'name': 'GETFIELD', 'feature': CF_USE1 | CF_USE2 | CF_USE3 },

        { 'name': 'GETADDRESS', 'feature': 0 },

        { 'name': 'GETSTKPOINTER', 'feature': CF_USE1 | CF_USE2 | CF_USE3 },
        { 'name': 'GETDATAPOINTER', 'feature': CF_USE1 | CF_USE2 | CF_USE3 },

        { 'name': 'LOAD_UCHAR', 'feature': CF_USE1 },
        { 'name': 'LOAD_SCHAR', 'feature': CF_USE1 },
        { 'name': 'LOAD_UHALF', 'feature': CF_USE1 },
        { 'name': 'LOAD_SHALF', 'feature': CF_USE1 },
        { 'name': 'LOAD_UWORD', 'feature': CF_USE1 },
        { 'name': 'LOAD_SWORD', 'feature': CF_USE1 },
        { 'name': 'LOAD_FLOAT', 'feature': CF_USE1 },
        { 'name': 'LOAD_DATAPTR', 'feature': CF_USE1 },

        { 'name': 'NOP', 'feature': 0 },
        { 'name': 'EXIT', 'feature': CF_STOP },
        { 'name': 'FORCETYPE', 'feature': CF_USE1 | CF_USE2 },

        { 'name': 'MOV', 'feature': 0 },

        { 'name': 'POPVAR', 'feature': 0 },

        { 'name': 'RESERVE', 'feature': CF_USE1 },
        { 'name': 'UNRESERVE', 'feature': CF_USE1 },
        { 'name': 'PUSH', 'feature': CF_USE1 },

        { 'name': 'SPAWN', 'feature': CF_USE1 | CF_USE2 | CF_CALL },

        { 'name': 'INCR', 'feature': 0 },
        { 'name': 'NEG', 'feature': 0 },
    ]
    instruc_start = 0
    instruc_end = len(instruc)

    def __init__(self):
        processor_t.__init__(self)

        self.inames = {}
        for idx, ins in enumerate(self.instrs):
            self.inames[ins['name']] = idx

        self.reg_ids = {}
        for idx, reg in enumerate(self.reg_names):
            self.reg_ids[reg] = idx

        self.regFirstSreg = self.regCodeSreg = self.reg_ids["CS"]
        self.regLastSreg = self.regDataSreg = self.reg_ids["DS"]

    def notify_init(self, idp_file):
        """Called at module initialization."""
        cvar.inf.mf = True  # set to big endian... wtf
        return True

    def _read_cmd_byte(self):
        ea = self.cmd.ea + self.cmd.size
        byte = get_full_byte(ea)
        self.cmd.size += 1
        return byte

    def _read_cmd_dword(self):
        hi = self._read_cmd_byte() << 24
        mh = self._read_cmd_byte() << 16
        ml = self._read_cmd_byte() << 8
        lo = self._read_cmd_byte()

        return hi | mh | ml | lo

    def _ana_deref(self, cmd, opcode):
        op = (opcode & 0x00FF0000) >> 16
        if op == 0x26:
            cmd.itype = self.inames["INDEX"]
        elif op == 0x27:
            cmd.itype = self.inames["DEREF"]
        elif op == 0x2A:
            cmd.itype = self.inames["RAWINDEX"]
            arg = self._read_cmd_dword()
            idx = arg & 0xFFFF
            elem_size = arg >> 16
            elem_type = opcode & 0xFF
            cmd[0].type = o_imm; cmd[0].dtyp = dt_word; cmd[0].value = idx
            cmd[1].type = o_imm; cmd[1].dtyp = dt_word; cmd[1].value = elem_size
            cmd[2].type = o_imm; cmd[2].dtyp = dt_byte; cmd[2].value = elem_type
        elif op == 0x2B:
            cmd.itype = self.inames["GETFIELD"]
            arg = self._read_cmd_dword()
            off = arg & 0xFFFF
            elem_size = arg >> 16
            elem_type = opcode & 0xFF
            cmd[0].type = o_imm; cmd[0].dtyp = dt_word; cmd[0].value = off
            cmd[1].type = o_imm; cmd[1].dtyp = dt_word; cmd[1].value = elem_size
            cmd[2].type = o_imm; cmd[2].dtyp = dt_byte; cmd[2].value = elem_type
        else:
            raise DecodingError()

    def _ana_01(self, cmd, opcode):
        ops = {
            0x00: "POPVAR",
            0x01: "INCR",
            0x05: "NEG",
            0x06: "NOP",
            0x09: "MOV",
            0x0A: "ADD",
            0x0B: "SUB",
            0x14: "CMPEQ",
            0x15: "CMPNE",
            0x16: "CMPLE",
            0x17: "CMPGE",
            0x18: "CMPGT",
            0x19: "CMPLT",
            0x1A: "AND",
            0x1B: "OR",
            0x1C: "MUL",
            0x1D: "DIV",
            0x1E: "MOD",
            0x1F: "ADD_NOUP",
            0x20: "SUB_NOUP",
            0x21: "AND",
            0x22: "OR",
            0x25: "SHR",
            0x28: "GETADDRESS",
        }
        op = (opcode & 0x00FF0000) >> 16
        if op == 0x29:
            cmd.itype = self.inames["CAST"]
            cmd[0].type = o_imm; cmd[0].dtyp = dt_byte; cmd[0].value = opcode & 0xFF
            return
        if op not in ops:
            return self._ana_deref(cmd, opcode)
        cmd.itype = self.inames[ops[op]]

    def _ana_load(self, cmd, opcode):
        types = {
            0x02: "UCHAR",
            0x03: "SCHAR",
            0x04: "UHALF",
            0x05: "SHALF",
            0x06: "UWORD",
            0x07: "SWORD",
            0x08: "FLOAT",
            0x82: "DATAPTR",
            0x84: "DATAPTR",
        }
        type = (opcode & 0x00FF0000) >> 16
        if type not in types:
            raise DecodingError()
        cmd.itype = self.inames["LOAD_" + types[type]]
        if types[type].endswith("CHAR"):
            t = dt_byte
            v = opcode & 0xFF
        elif types[type].endswith("HALF"):
            t = dt_word
            v = opcode & 0xFFFF
        elif types[type].endswith("WORD"):
            t = dt_dword
            v = self._read_cmd_dword()
        elif types[type].endswith("FLOAT"):
            t = dt_dword
            v = self._read_cmd_dword()
        elif types[type].endswith("DATAPTR"):
            offset = self._read_cmd_dword()
            cmd[0].type = o_mem; cmd[0].dtyp = dt_byte; cmd[0].addr = 0x08000000 + offset
            return
        cmd[0].type = o_imm; cmd[0].dtyp = t; cmd[0].value = v

    def _ana(self):
        cmd = self.cmd
        opcode = self._read_cmd_dword()

        hi = (opcode & 0xFF000000) >> 24
        if hi == 0x01:
            self._ana_01(cmd, opcode)
        elif hi == 0x02:
            self._ana_load(cmd, opcode)
        elif hi == 0x03:
            cmd.itype = self.inames["GETSTKPOINTER"]
            type = (opcode >> 16) & 0xFF
            pointed_size = (opcode & 0xFFFF)
            offset = self._read_cmd_dword()
            cmd[0].type = o_imm; cmd[0].dtyp = dt_byte; cmd[0].value = type
            cmd[1].type = o_imm; cmd[1].dtyp = dt_word; cmd[1].value = pointed_size
            cmd[2].type = o_imm; cmd[2].dtyp = dt_dword; cmd[2].value = offset
        elif hi == 0x04:
            cmd.itype = self.inames["GETDATAPOINTER"]
            type = (opcode >> 16) & 0xFF
            pointed_size = (opcode & 0xFFFF)
            offset = self._read_cmd_dword()
            cmd[0].type = o_imm; cmd[0].dtyp = dt_byte; cmd[0].value = type
            cmd[1].type = o_imm; cmd[1].dtyp = dt_word; cmd[1].value = pointed_size
            cmd[2].type = o_mem; cmd[2].dtyp = dt_dword; cmd[2].addr = 0x08000000 + offset
        elif hi == 0x05:
            target = self._read_cmd_dword()
            if target == 0xFFFFFFFF:
                cmd.itype = self.inames["SYSCALL"]
                syscall_id = opcode & 0xFFFF
                nargs = (opcode & 0x00FF0000) >> 16
                cmd[0].type = o_imm; cmd[0].dtyp = dt_word; cmd[0].value = syscall_id
                cmd[1].type = o_imm; cmd[1].dtyp = dt_byte; cmd[1].value = nargs
            else:
                cmd.itype = self.inames["CALL"]
                cmd[0].type = o_near; cmd[0].dtyp = dt_dword; cmd[0].addr = 0x02000000 + target
        elif hi == 0x06:
            cmd.itype = self.inames["RET"]
            type = (opcode & 0x00FF0000) >> 16
            cmd[0].type = o_imm; cmd[0].dtyp = dt_byte; cmd[0].value = type
        elif hi == 0x07:
            cmd.itype = self.inames["EXIT"]
        elif hi == 0x08:
            cmd.itype = self.inames["J"]
            target = self._read_cmd_dword()
            cmd[0].type = o_near; cmd[0].dtyp = dt_dword; cmd[0].addr = 0x02000000 + target
        elif hi == 0x09:
            cmd.itype = self.inames["JZ"]
            target = self._read_cmd_dword()
            cmd[0].type = o_near; cmd[0].dtyp = dt_dword; cmd[0].addr = 0x02000000 + target
        elif hi == 0x0E:
            cmd.itype = self.inames["PUSH"]
            type = opcode & 0xFFFF
            cmd[0].type = o_imm; cmd[0].dtyp = dt_word; cmd[0].value = type
        elif hi == 0x10:
            cmd.itype = self.inames["UNRESERVE"]
            val = self._read_cmd_dword()
            cmd[0].type = o_imm; cmd[0].dtyp = dt_dword; cmd[0].value = val
        elif hi == 0x11:
            cmd.itype = self.inames["RESERVE"]
            val = self._read_cmd_dword()
            cmd[0].type = o_imm; cmd[0].dtyp = dt_dword; cmd[0].value = val
        elif hi == 0x13:
            cmd.itype = self.inames["SPAWN"]
            target = self._read_cmd_dword()
            nargs = (opcode & 0x00FF0000) >> 16
            cmd[0].type = o_near; cmd[0].dtyp = dt_dword; cmd[0].addr = 0x02000000 + target
            cmd[1].type = o_imm; cmd[1].dtyp = dt_byte; cmd[1].value = nargs
        elif hi == 0x14:
            cmd.itype = self.inames["FORCETYPE"]
            type = (opcode >> 16) & 0xFF
            pointed_size = (opcode & 0xFFFF)
            cmd[0].type = o_imm; cmd[0].dtyp = dt_byte; cmd[0].value = type
            cmd[1].type = o_imm; cmd[1].dtyp = dt_word; cmd[1].value = pointed_size
        else:
            raise DecodingError()

        return cmd.size

    def ana(self):
        """Analyze one instruction and fill the "cmd" instance member."""
        try:
            return self._ana()
        except DecodingError:
            return 0

    def _emu_operand(self, op):
        if op.type == o_mem:
            ua_dodata2(0, op.addr, op.dtyp)
            ua_add_dref(0, op.addr, dr_R)
        elif op.type == o_near:
            if self.cmd.get_canon_feature() & CF_CALL:
                fl = fl_CN
            else:
                fl = fl_JN
            ua_add_cref(0, op.addr, fl)

    def emu(self):
        """Emulate instruction behavior and create x-refs, interpret operand
        values, etc."""
        cmd = self.cmd
        ft = cmd.get_canon_feature()

        if ft & CF_USE1:
            self._emu_operand(cmd[0])
        if ft & CF_USE2:
            self._emu_operand(cmd[1])
        if ft & CF_USE3:
            self._emu_operand(cmd[2])

        if not ft & CF_STOP:
            ua_add_cref(0, cmd.ea + cmd.size, fl_F)

        return True

    def outop(self, op):
        """Generates text representation of an instruction operand."""
        if op.type == o_imm:
            OutValue(op, OOFW_IMM)
        elif op.type in [o_near, o_mem]:
            ok = out_name_expr(op, op.addr, BADADDR)
            if not ok:
                out_tagon(COLOR_ERROR)
                OutLong(op.addr, 16)
                out_tagoff(COLOR_ERROR)
                QueueMark(Q_noName, self.cmd.ea)
        else:
            return False
        return True

    def out(self):
        """Generates text representation of an instruction in the "cmd" inst
        member."""
        cmd = self.cmd
        ft = cmd.get_canon_feature()

        buf = init_output_buffer(1024)
        OutMnem(15)

        if ft & CF_USE1:
            out_one_operand(0)
        if ft & CF_USE2:
            OutChar(',')
            OutChar(' ')
            out_one_operand(1)
        if ft & CF_USE3:
            OutChar(',')
            OutChar(' ')
            out_one_operand(2)

        term_output_buffer()
        cvar.gl_comm = 1
        MakeLine(buf)

def PROCESSOR_ENTRY():
    return CScriptProcessor()

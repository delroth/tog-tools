# IDA File Loader for Wii "Tales of" games bytecode files.

from idaapi import *
from idc import *

import struct

def dword_at(input, off):
    """Utility function to read a BE uint32 at the given offset."""
    input.seek(off)
    s = input.read(4)
    return struct.unpack(">I", s)[0]

def bytes_at(input, start, end):
    input.seek(start)
    return input.read(end - start)

def accept_file(input, n):
    if n > 0:
        return 0 # We only support one format

    if input.size() < 0x20:
        return 0 # File too small

    if input.read(4) != "TSS\x00":
        return 0 # Bad magic

    code_base = dword_at(input, 0x04)
    entry_point = dword_at(input, 0x08)
    data_base = dword_at(input, 0x0C)
    data_size = dword_at(input, 0x18)

    if code_base > input.size():
        return 0
    if data_base > input.size():
        return 0
    if data_base < code_base:
        return 0
    if data_base + data_size > input.size():
        return 0
    if entry_point % 4 != 0:
        return 0
    if entry_point > data_base - code_base:
        return 0

    return 'Wii "Tales of" games bytecode'

def load_file(input, neflags, format):
    set_processor_type("cscript", SETPROC_ALL | SETPROC_FATAL)

    code_base = dword_at(input, 0x04)
    entry_point = dword_at(input, 0x08) + 0x02000000
    data_base = dword_at(input, 0x0C)
    data_size = dword_at(input, 0x18)

    code_seg = segment_t()
    code_seg.startEA = 0x02000000
    code_seg.endEA = 0x02000000 + data_base - code_base
    code_seg.bitness = 1
    add_segm_ex(code_seg, "text", "CODE", 0)
    mem2base(bytes_at(input, code_base, data_base),
             0x02000000, 0x02000000 + data_base - code_base)
    add_entry(entry_point, entry_point, "_start", True)

    data_seg = segment_t()
    data_seg.startEA = 0x08000000
    data_seg.endEA = 0x08000000 + data_size
    data_seg.bitness = 1
    add_segm_ex(data_seg, "data", "DATA", 0)
    mem2base(bytes_at(input, data_base, data_base + data_size),
             0x08000000, 0x08000000 + data_size)

    for i in xrange(0x20):
        start = 0x04000000 | (i << 16)
        end = start + 0x800

        stack_seg = segment_t()
        stack_seg.startEA = start
        stack_seg.endEA = end
        stack_seg.bitness = 1
        add_segm_ex(stack_seg, "stack%d" % i, "STACK", 0)

    return True

#! /usr/bin/env python3

import compression.cpk

if __name__ == '__main__':
    data = open('CHT_SB002.chd', 'rb').read()
    ext_size = 1762240
    open('out.bin', 'wb').write(compression.cpk.uncompress(data, ext_size))

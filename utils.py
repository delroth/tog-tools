import copy
import os
import os.path
import struct

class BinaryStruct:
    def __init__(self, big_endian=False, fields=()):
        if len(fields) == 0:
            raise ValueError("zero size binary struct")

        self._fields = fields

        fmt_str = ">" if big_endian else "<"
        for (field_name, field_type) in fields:
            fmt_str += field_type
        self._fmt_str = fmt_str

    def __str__(self):
        return "<" + ', '.join("%r: %r" % (n, getattr(self, n))
                               for (n, _) in self._fields) + ">"

    @property
    def size(self):
        return struct.calcsize(self._fmt_str)

    def unpack(self, buffer):
        values = struct.unpack(self._fmt_str, buffer)
        for ((name, type), val) in zip(self._fields, values):
            setattr(self, name, val)

    def pack(self):
        values = [getattr(self, name) for (name, type) in self._fields]
        return struct.pack(self._fmt_str, *values)

    def __call__(self):
        return copy.deepcopy(self)

class FileUnpacker:
    def __init__(self, fp):
        self._fp = fp

    def read_at(self, offset, size):
        self._fp.seek(offset)
        return self._fp.read(size)

    def parse_cstring_at(self, offset, maxsize=4096):
        buf = self.read_at(offset, maxsize)
        return buf[:buf.find(b'\0')]

    def parse_struct_at(self, offset, struct):
        ret = struct()
        ret.unpack(self.read_at(offset, ret.size))
        return ret

    def check(self):
        if not hasattr(self, "magic"):
            return False
        return self.read_at(0, len(self.magic)) == self.magic

    def output_file(self, path, data):
        print("Extracting %s (%d bytes)..." % (path, len(data)))

        dirname = os.path.dirname(path)
        try:
            os.makedirs(dirname)
        except os.error:
            pass
        fp = open(path, "wb")
        fp.write(data)
        fp.close()

class FilePacker:
    def __init__(self, fp):
        self._fp = fp

    def write_at(self, offset, data):
        self._fp.seek(offset)
        self._fp.write(data)

    def pack_struct(self, offset, struct):
        data = struct.pack()
        self.write_at(offset, data)

def get_all_files(indir):
    for (dir, dirnames, filenames) in os.walk(indir):
        for filename in filenames:
            relpath = os.path.join(os.path.relpath(dir, indir), filename)
            if relpath.startswith("./"):
                relpath = relpath[2:]
            fullpath = os.path.join(dir, filename)
            size = os.path.getsize(fullpath)
            yield relpath, fullpath, size

PYX=$(shell find . -name "*.pyx")
C=$(PYX:.pyx=_cython.c)
SO=$(PYX:.pyx=.so)

CC=gcc
CFLAGS=-O2 -shared -fPIC

CYTHON?=cython -3
PYTHON?=python3
CFLAGS+=$(shell $(PYTHON)-config --cflags)

all: all-so

all-so: $(SO)

%.so: %_cython.c
	$(CC) $(CFLAGS) -o $@ $<

%_cython.c: %.pyx
	$(CYTHON) -o $@ $<

clean:

distclean: clean clean-so

clean-so:
	rm -f $(SO)

#! /usr/bin/env python3

import format.scs
import os
import os.path
import sys

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('usage: %s <jp-dir> <us-dir> <out-dir>' % sys.argv[0])
        sys.exit(0)

    jp_dir, us_dir, out_dir = sys.argv[1:]
    for skit_file in os.listdir(jp_dir):
        if skit_file.startswith("debug_"):
            print("%s: skipping (debug)" % skit_file)
            continue

        jp_skit = os.path.join(jp_dir, skit_file)
        us_skit = os.path.join(us_dir, skit_file)

        if not os.path.exists(us_skit):
            print("%s: no US equivalent" % skit_file)
            continue

        jp_scs = format.scs.SCS.from_file(jp_skit)
        us_scs = format.scs.SCS.from_file(us_skit)

        if len(jp_scs.strings) != len(us_scs.strings) - 2:
            print("%s: not the same amount of strings" % skit_file)
            continue

        merged_strs = jp_scs.strings[0:11] + us_scs.strings[13:]
        merged_scs = format.scs.SCS(merged_strs)
        merged_scs.to_file(os.path.join(out_dir, skit_file))

        print("%s: merged" % skit_file)

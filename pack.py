#! /usr/bin/env python3

import format
import sys

if __name__ == '__main__':
    if len(sys.argv) != 4 or sys.argv[1] not in format.formats:
        print('usage: %s <format> <outfile> <infile|indir>' % sys.argv[0])
        print('Allowed formats: %s' % ','.join(sorted(format.formats.keys())))
        sys.exit(0)

    mod = format.formats[sys.argv[1]]
    fp = open(sys.argv[2], "wb")
    unpacker = mod.Packer(fp)
    unpacker.pack(sys.argv[3])

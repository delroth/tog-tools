"""
SCS packer/unpacker. Very simple format used to store strings from scripts (and
probably some other places).
"""

import struct
import utils

class SCS:
    def __init__(self, strings=None):
        self.strings = strings if strings is not None else []

    def unpack(self, up, off=0):
        nstrings = struct.unpack(">I", up.read_at(off, 4))[0]
        off += 4

        string_offsets = []
        for i in range(nstrings):
            stroff = struct.unpack(">I", up.read_at(off, 4))[0]
            string_offsets.append(stroff)
            off += 4

        self.strings = [up.parse_cstring_at(off) for off in string_offsets]

    def pack(self, up, off=0):
        start_off = off
        up.write_at(off, struct.pack(">I", len(self.strings)))

        strtbl = b'\x00'.join(self.strings) + b'\x00'

        str_off = start_off + 4 + 4 * len(self.strings)
        offsets = []
        for s in self.strings:
            offsets.append(str_off)
            str_off += len(s) + 1

        for idx, off in enumerate(offsets):
            up.write_at(start_off + 4 + 4 * idx,
                        struct.pack(">I", off))

        up.write_at(start_off + 4 + 4 * len(offsets), strtbl)

    @staticmethod
    def from_file(filename):
        s = SCS()
        s.unpack(utils.FileUnpacker(open(filename, "rb")))
        return s

    def to_file(self, filename):
        self.pack(utils.FilePacker(open(filename, "wb")))

"""
CPK format packer/unpacker. Inspired by the CRI CPK archive script for QuickBMS
(which handles unpacking but no repacking), which is itself derived from
cpk_unpack (http://hcs64.com/vgm_ripping.html).
"""

import compression.cpk
import os.path
import struct
import utils

class UTFColumn:
    ColumnSchema = utils.BinaryStruct(
        big_endian = True,
        fields = (
            ("type", "B"),
            ("name_offset", "I"),
        )
    )

    # Data type stored in a column
    DATA_TYPES = {
        "U8": 0x0,
        "S8": 0x1,
        "U16": 0x2,
        "S16": 0x3,
        "U32": 0x4,
        "S32": 0x5,
        "U64": 0x6,
        "S64": 0x7,
        "FLOAT": 0x8,
        "STRING": 0xA,
        "BLOB": 0xB
    }

    DATA_TYPES_SIZE = {
        0x0: 1,
        0x1: 1,
        0x2: 2,
        0x3: 2,
        0x4: 4,
        0x5: 4,
        0x6: 8,
        0x7: 8,
        0x8: 4,
        0xA: 4,
        0xB: 8
    }

    # How the data is stored
    STORAGE_TYPES = {
        "ZERO": 0x1,
        "CONSTANT": 0x3,
        "PER_ROW": 0x5
    }

    def __init__(self, name="UnknownColumn", type=0x10, constant=None):
        self.name = name
        self.type = type
        self.constant = constant

    @property
    def data_type(self):
        return self.type & 0x0F

    @property
    def storage_type(self):
        return self.type >> 4

    @property
    def schema_size(self):
        size = UTFColumn.ColumnSchema.size
        if self.storage_type == UTFColumn.STORAGE_TYPES["CONSTANT"]:
            size += UTFColumn.DATA_TYPES_SIZE[self.data_type]
        return size

    @property
    def per_row_size(self):
        if self.storage_type == UTFColumn.STORAGE_TYPES["PER_ROW"]:
            return UTFColumn.DATA_TYPES_SIZE[self.data_type]
        return 0

    def unpack(self, up, utf, offset):
        col = up.parse_struct_at(offset, UTFColumn.ColumnSchema)
        offset += UTFColumn.ColumnSchema.size

        self.name = utf.unpack_string(up, col.name_offset)
        self.type = col.type
        if self.storage_type == UTFColumn.STORAGE_TYPES["CONSTANT"]:
            self.constant = self.unpack_data(up, utf, offset)
        return self.schema_size

    def pack(self, up, str_offsets, offset):
        col = UTFColumn.ColumnSchema()
        col.type = self.type
        col.name_offset = str_offsets[self.name]
        up.pack_struct(offset, col)
        offset += col.size

        if self.storage_type == UTFColumn.STORAGE_TYPES["CONSTANT"]:
            self.pack_data(up, str_offsets, offset, self.constant, force=True)

    def unpack_data(self, up, utf, offset):
        if self.constant is not None:
            return self.constant
        elif self.storage_type == UTFColumn.STORAGE_TYPES["ZERO"]:
            if self.data_type == UTFColumn.DATA_TYPES["STRING"]:
                return ""
            elif self.data_type == UTFColumn.DATA_TYPES["FLOAT"]:
                return 0.0
            else:
                return 0

        data = up.read_at(offset, UTFColumn.DATA_TYPES_SIZE[self.data_type])
        trivial_types = {
            0x0: ">B",
            0x1: ">b",
            0x2: ">H",
            0x3: ">h",
            0x4: ">I",
            0x5: ">i",
            0x6: ">Q",
            0x7: ">q",
            0x8: ">f"
        }
        if self.data_type in trivial_types:
            return struct.unpack(trivial_types[self.data_type], data)[0]
        elif self.data_type == UTFColumn.DATA_TYPES["STRING"]:
            off = struct.unpack(">I", data)[0]
            return utf.unpack_string(up, off)
        elif self.data_type == UTFColumn.DATA_TYPES["BLOB"]:
            off, size = struct.unpack(">II", data)
            return utf.unpack_blob(up, off, size)

    def pack_data(self, up, str_offsets, offset, val, force=False):
        if self.storage_type == UTFColumn.STORAGE_TYPES["CONSTANT"] \
                and not force:
            return
        elif self.storage_type == UTFColumn.STORAGE_TYPES["ZERO"]:
            return
        else:
            # TODO: refactor
            trivial_types = {
                0x0: ">B",
                0x1: ">b",
                0x2: ">H",
                0x3: ">h",
                0x4: ">I",
                0x5: ">i",
                0x6: ">Q",
                0x7: ">q",
                0x8: ">f"
            }
            if self.data_type in trivial_types:
                up.write_at(offset, struct.pack(trivial_types[self.data_type],
                                                val))
            elif self.data_type == UTFColumn.DATA_TYPES["STRING"]:
                val = str_offsets[val]
                up.write_at(offset, struct.pack(">I", val))

    def __str__(self):
        for (name, val) in UTFColumn.DATA_TYPES.items():
            if val == self.data_type:
                data_type_name = name
                break
        else:
            data_type_name = "UNK/%1x" % self.data_type
        for (name, val) in UTFColumn.STORAGE_TYPES.items():
            if val == self.storage_type:
                storage_type_name = name
                break
        else:
            storage_type_name = "UNK/%1x" % self.storage_type
        return ("<UTFColumn: %r - data %s, storage %s%s>" % (
            self.name, data_type_name, storage_type_name,
            "" if self.constant is None else ", const %r" % self.constant
        ))

    def __repr__(self):
        return str(self)

class UTF:
    Header = utils.BinaryStruct(
        big_endian = True,
        fields = (
            ("table_size", "I"),
            ("rows_off", "I"),
            ("strtbl_off", "I"),
            ("data_off", "I"),
            ("table_name_off", "I"),
            ("columns_cnt", "H"),
            ("row_width", "H"),
            ("rows_cnt", "I"),
        )
    )

    def __init__(self, name="UnknownTable", schema=None, rows=None,
                 magic=None):
        self.name = name
        self.schema = [] if schema is None else schema
        self.rows = [] if rows is None else rows
        self.magic = magic
        self.size = None

    def unpack(self, up, offset):
        self.magic = up.read_at(offset, 4)
        offset += 0x8
        self.size = struct.unpack(">I", up.read_at(offset, 4))[0]
        offset += 0x8
        if up.read_at(offset, 4) != b"@UTF":
            raise utils.UnpackError(
                "Could not find a valid UTF structure at %d" % offset
            )
        offset += 4

        self.header = header = up.parse_struct_at(offset, UTF.Header)
        header.strtbl_off += 4 + offset
        header.rows_off += 4 + offset
        header.data_off += 4 + offset
        self.name = self.unpack_string(up, header.table_name_off)
        offset += UTF.Header.size

        for colidx in range(header.columns_cnt):
            col = UTFColumn()
            col.unpack(up, self, offset)
            self.schema.append(col)
            offset += col.schema_size

        offset = header.rows_off
        for rowidx in range(header.rows_cnt):
            row = {}
            for col in self.schema:
                row[col.name] = col.unpack_data(up, self, offset)
                offset += col.per_row_size
            self.rows.append(row)

    def unpack_string(self, up, off):
        return up.parse_cstring_at(self.header.strtbl_off + off).decode('utf-8')

    def unpack_blob(self, up, off, size):
        return up.read_at(self.header.data_off + off, size)

    def pack(self, up, off):
        start_off = off
        strtbl, str_offsets = self.make_strtbl()

        up.write_at(off, self.magic + b"\xFF")
        size_off = off + 8
        off += 0x10

        up.write_at(off, b"@UTF")
        header_off = off + 4
        off = header_off + UTF.Header.size

        for col in self.schema:
            col.pack(up, str_offsets, off)
            off += col.schema_size

        rows_off = off
        for row in self.rows:
            for col in self.schema:
                col.pack_data(up, str_offsets, off, row[col.name])
                off += col.per_row_size

        strtbl_off = off
        up.write_at(strtbl_off, strtbl)

        self.size = (off + len(strtbl)) - start_off
        self.size = (self.size + 31) & ~31
        up.write_at(size_off, struct.pack(">I", self.size))

        header = UTF.Header()
        header.table_size = self.size - 0x8
        header.rows_off = rows_off - start_off - 0x18
        header.strtbl_off = strtbl_off - start_off - 0x18
        header.data_off = start_off + self.size - 0x18
        header.table_name_off = str_offsets[self.name]
        header.columns_cnt = len(self.schema)
        header.row_width = sum(col.per_row_size for col in self.schema)
        header.rows_cnt = len(self.rows)
        up.pack_struct(header_off, header)

    def make_strtbl(self):
        strings = set()
        strings.add(self.name)

        strcols = []
        for col in self.schema:
            strings.add(col.name)
            if col.data_type == UTFColumn.DATA_TYPES["STRING"]:
                if col.storage_type == UTFColumn.STORAGE_TYPES["CONSTANT"]:
                    strings.add(col.constant)
                elif col.storage_type == UTFColumn.STORAGE_TYPES["PER_ROW"]:
                    strcols.append(col.name)

        for row in self.rows:
            for col in strcols:
                strings.add(row[col])

        data = [s.encode('utf-8') for s in strings]
        strings = list(strings)
        offsets = {}

        strtbl = b'NULL\x00' + b'\x00'.join(data) + b'\x00'
        cur_off = 5
        for s, d in zip(strings, data):
            offsets[s] = cur_off
            cur_off += len(d) + 1
        return strtbl, offsets

    def __getitem__(self, idx):
        return self.rows[idx]

    def __setitem__(self, idx, val):
        self.rows[idx] = val

    def __str__(self):
        return "<UTF: %r, %d columns, %d rows>" % (
            self.name, len(self.schema), len(self.rows)
        )

class Unpacker(utils.FileUnpacker):
    magic = b"CPK "

    def unpack(self, outdir):
        header = UTF()
        header.unpack(self, 0)

        toc_off = header[0]["TocOffset"]
        content_off = header[0]["ContentOffset"]

        if self.read_at(toc_off, 4) != b"TOC ":
            raise utils.UnpackError(
                    "Invalid TOC offset: %d" % toc_off
            )

        toc = UTF()
        toc.unpack(self, toc_off)

        for row in toc.rows:
            dirname = row["DirName"]
            filename = row["FileName"]
            filesize = row["FileSize"]
            extractsize = row["ExtractSize"]
            fileoffset = row["FileOffset"]

            path = os.path.join(outdir, dirname, filename)
            data = self.read_at(content_off + fileoffset, filesize)
            if extractsize > filesize:
                data = compression.cpk.uncompress(data, extractsize)
            self.output_file(path, data)

class Packer(utils.FilePacker):
    def make_strtbl(self, strings):
        l = list(strings)
        strtbl = b'\x00'.join(l)
        offsets = {}
        cur = 0
        for s in l:
            offsets[s] = cur
            cur += len(s) + 1
        return strtbl, offsets

    def pack(self, indir):
        content_off = 0x800
        rows = []
        for relpath, fullpath, size in utils.get_all_files(indir):
            # TODO: compress data
            print("Storing %s (%d bytes)..." % (relpath, size))
            self.write_at(content_off, open(fullpath, 'rb').read())
            rows.append({
                "DirName": os.path.dirname(relpath),
                "FileName": os.path.basename(relpath),
                "FileSize": size,
                "ExtractSize": 0,
                "FileOffset": content_off - 0x800
            })
            content_off += size
            content_off = (content_off + 31) & ~31

        toc_off = content_off
        toc = UTF("CpkTocInfo", [
                UTFColumn("DirName", 0x5A),
                UTFColumn("FileName", 0x5A),
                UTFColumn("FileSize", 0x54),
                UTFColumn("ExtractSize", 0x54),
                UTFColumn("FileOffset", 0x56),
            ], rows, magic=b"TOC "
        )
        toc.pack(self, toc_off)

        hdr = UTF("CpkHeader", [
                UTFColumn("UpdateDateTime", 0x56),
                UTFColumn("FileSize", 0x56),
                UTFColumn("ContentOffset", 0x56),
                UTFColumn("ContentSize", 0x56),
                UTFColumn("TocOffset", 0x56),
                UTFColumn("TocSize", 0x56),
                UTFColumn("EtocOffset", 0x56),
                UTFColumn("EtocSize", 0x56),
                UTFColumn("ItocOffset", 0x56),
                UTFColumn("ItocSize", 0x56),
                UTFColumn("GtocOffset", 0x56),
                UTFColumn("GtocSize", 0x56),
                UTFColumn("Version", 0x52),
                UTFColumn("Revision", 0x52),
                UTFColumn("Align", 0x52),
                UTFColumn("Sorted", 0x52),
            ], [{
                "UpdateDateTime": 0,
                "FileSize": 0,
                "ContentOffset": 0x800,
                "ContentSize": 0,
                "TocOffset": toc_off,
                "TocSize": toc.size,
                "EtocOffset": 0,
                "EtocSize": 0,
                "ItocOffset": 0,
                "ItocSize": 0,
                "GtocOffset": 0,
                "GtocSize": 0,
                "Version": 3,
                "Revision": 0,
                "Align": 4,
                "Sorted": 0,
            }], magic=b"CPK "
        )
        hdr.pack(self, 0)

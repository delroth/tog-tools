#! /usr/bin/env python

import format.scs
import sys

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: %s <scs-file>' % sys.argv[0])
        sys.exit(0)

    scs = format.scs.SCS.from_file(sys.argv[1])
    print("%d strings" % len(scs.strings))
    for idx, str in enumerate(scs.strings):
        print("%d: %s" % (idx, str.decode('sjis')))

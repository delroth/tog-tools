cdef char next_bit(char* input, size_t* input_pos, char* rembits,
                   char* rembits_cnt):
    if rembits_cnt[0] == 0:
        input_pos[0] -= 1
        rembits[0] = input[input_pos[0]]
        rembits_cnt[0] = 8

    cdef unsigned char res = rembits[0] >> 7
    rembits[0] <<= 1
    rembits_cnt[0] -= 1
    return res & 1

cdef size_t next_bits(size_t cnt, char* input, size_t* input_pos,
                      char* rembits, char* rembits_cnt):
    cdef size_t ret = 0
    cdef size_t i
    for i from 0 <= i < cnt:
        ret <<= 1
        ret |= next_bit(input, input_pos, rembits, rembits_cnt)
    return ret

def uncompress(bytes input_py, size_t final_size):
    cdef bytes output_py = b'\x00' * final_size
    cdef char* output = output_py
    cdef char* input = input_py
    cdef size_t output_pos = final_size
    cdef size_t input_pos = len(input_py) - 0x100

    cdef char rembits = 0
    cdef char rembits_cnt = 0

    cdef size_t backref_pos, backref_len, cur_len

    cdef size_t lengths[4]
    lengths[0] = 2
    lengths[1] = 3
    lengths[2] = 5
    lengths[3] = 8

    cdef size_t i, level
    for i from 0 <= i < 0x100:
        output[i] = input[input_pos + i]

    while output_pos > 0x100:
        if input_pos == 0:
            break

        if next_bit(input, &input_pos, &rembits, &rembits_cnt):
            backref_pos = output_pos
            backref_pos += next_bits(13, input, &input_pos, &rembits,
                                     &rembits_cnt)
            backref_pos += 3

            backref_len = 3

            for level from 0 <= level < 4:
                cur_len = next_bits(lengths[level], input, &input_pos,
                                    &rembits, &rembits_cnt)
                backref_len += cur_len
                if cur_len != ((1 << lengths[level]) - 1):
                    break
            else:
                while True:
                    cur_len = next_bits(8, input, &input_pos, &rembits,
                                        &rembits_cnt)
                    backref_len += cur_len
                    if cur_len != 0xFF:
                        break

            for i from 0 <= i < backref_len:
                output_pos -= 1
                backref_pos -= 1
                output[output_pos] = output[backref_pos]
        else:
            output_pos -= 1
            output[output_pos] = next_bits(8, input, &input_pos, &rembits,
                                           &rembits_cnt)

    return output_py
